del formula.jar
del src\org\formula\*.class
javac src\org\formula\*.java
mkdir tmp\org\formula\
move src\org\formula\*.class tmp\org\formula\
jar --create --file formula.jar --main-class=org.formula.Demo -C tmp .
del tmp /S /Q
rmdir tmp\org\formula
rmdir tmp\org
rmdir tmp
pause