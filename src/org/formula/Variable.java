package org.formula;

/**
 *
 * @author MAndrejevic
 */
public class Variable implements Operation{

    private Operation _parent;
    private VariableValue _value;
    
    public Variable(VariableValue iVar){
        _parent = null;
        _value = iVar;
    }
    
    @Override
    public double result(){
        if(Double.isNaN(_value.getValue())){
            throw new FormulaException("Der Variablen '" + _value.getName()
                    + "' wurde kein Wert zugewiesen.");
        }else{
            return _value.getValue();
        }
    }

    @Override
    public OpType type() {
        return OpType.VARIABLE;
    }

    @Override
    public void setParent(Operation iParentOp) {
        _parent = iParentOp;
    }
    
    @Override
    public int rank() {
        return 0;
    }

    @Override
    public void insertNewOperation(Operation iNewOp){
        try{
            _parent.insertNewOperation(iNewOp);
        }catch(NullPointerException e){
            throw new FormulaException("Programmierfehler:"
                    + " Variable.insertNewOperation wurde aufgerufen, bevor"
                    + " diese Instanz in den Formel-Baum eingefügt wurde.");
        }
    }
    
    @Override
    public void validateEnd(){} //Formel ok
    
    public static String readNextVariable(String iFormula, int iStringPos){
        int varEnd = iStringPos + 1;
        char next;
        
        while(varEnd < iFormula.length()){
            next = iFormula.charAt(varEnd);
            if(('A' <= next && next <= 'Z') || ('a' <= next && next <= 'z')
                    || ('0' <= next && next <= '9') || next == '_'){
                varEnd++;
            }else{
                break;
            }
        }
        
        return iFormula.substring(iStringPos, varEnd);
    }

}
