package org.formula;

/**
 *
 * @author MAndrejevic
 */
public class Scalar implements Operation{

    private Operation _parent;
    private double _value;
    
    public Scalar(double iVal){
        _parent = null;
        _value = iVal;
    }
    
    @Override
    public double result() {
        return _value;
    }

    @Override
    public OpType type() {
        return OpType.SCALAR;
    }

    @Override
    public void setParent(Operation iParentOp) {
        _parent = iParentOp;
    }
    
    @Override
    public int rank() {
        return 0;
    }

    @Override
    public void insertNewOperation(Operation iNewOp){
        try{
            _parent.insertNewOperation(iNewOp);
        }catch(NullPointerException e){
            throw new FormulaException("Programmierfehler:"
                    + " Scalar.insertNewOperation wurde aufgerufen, bevor"
                    + " diese Instanz in den Formel-Baum eingefügt wurde.");
        }
    }
    
    @Override
    public void validateEnd(){} //Formel ok
    
    public static String readNextScalar(String iFormula, int iStringPos){
        boolean decPoint = false;
        boolean mantEnd = false;
        int scalarEnd = iStringPos;
        char next;
        char last;
        
        while(scalarEnd < iFormula.length()){
            next = iFormula.charAt(scalarEnd);
            
            if('0' <= next && next <= '9'){
                scalarEnd++;
            }else if(next == '.'){
                if(decPoint){
                    throw new FormulaException("Zeichen " + (scalarEnd+1)
                            + ": zweiter Dezimalpunkt in Zahl");
                }else{
                    decPoint = true;
                    scalarEnd++;
                }
            }else if(next == 'E' || next == 'e'){
                if(mantEnd){
                    throw new FormulaException("Zeichen " + (scalarEnd+1)
                            + ": zweites Mantissenende in Zahl");
                }else{
                    mantEnd = true;
                    scalarEnd++;
                }
            }else if(next == '-' || next == '+'){
                if(iFormula.charAt(scalarEnd - 1) == 'E'
                        || iFormula.charAt(scalarEnd - 1) == 'e'){
                    scalarEnd++;
                }else{
                    break;
                }
            }else{
                break;
            }
        }
        
        last = iFormula.charAt(scalarEnd - 1);
        if(last == '+' || last == '-' || last == 'E' || last == 'e'){
            throw new FormulaException("Zeichen " + scalarEnd + ": Zahl endet mit '"
                    + last + "'. Am Ende einer Zahl wird eine Ziffer erwartet.");
        }
        
        return iFormula.substring(iStringPos, scalarEnd);
    }

}
