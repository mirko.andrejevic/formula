package org.formula;

/**
 *
 * @author MAndrejevic
 */
public class Brackets implements Operation{

    private Operation _parent;
    private Operation _child;
    private boolean _closed;
    
    public Brackets(){
        _parent = null;
        _child = null;
        _closed = false;
    }
    
    @Override
    public double result(){
        return _child.result();
    }

    @Override
    public OpType type() {
        return OpType.BRACKETS;
    }

    @Override
    public void setParent(Operation iParentOp) {
        _parent = iParentOp;
    }

    @Override
    public int rank() {
        return 0;
    }
    
    @Override
    public void insertNewOperation(Operation iNewOp){
        if(_closed){
            _parent.insertNewOperation(iNewOp);
        }else if(iNewOp.type() == OpType.CLOSEBRACKET){
            if(_child == null){
                throw new FormulaException("leere Klammern");
            }else{
                iNewOp.setParent(this);
                _closed = true;
            }
        }else if(iNewOp.type() == OpType.COMMA){
            throw new FormulaException("Komma außerhalb von"
                    + " Funktionsauswertung");
        }else if(_child == null){
            _child = iNewOp;
            _child.setParent(this);
        }else{
            iNewOp.insertNewOperation(_child);
            _child = iNewOp;
            _child.setParent(this);
        }
    }

    @Override
    public void validateEnd(){
        if(_child == null){
            throw new FormulaException("Die Formel endet mit '('.");
        }else if(!_closed){
            throw new FormulaException("Eine öffnende Klammer wurde nicht"
                    + " geschlossen");
        }
    }

}
