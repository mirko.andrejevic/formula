package org.formula;

import java.lang.System.*;

/**
 *
 * @author MAndrejevic
 */
public class Demo {
    
    public static void main(String[] args) {
        // example 1
        FormulaBuilder f1 = new FormulaBuilder("1/@e*$x*ln($x^2)");
        f1.setVariable("$x", Math.E);
        System.out.println("/------------------------");
        System.out.println("f($x) = " + f1.toString());
        System.out.println("f(@e) = " + f1.result());
        System.out.println("/------------------------");
        
        // example 2
        System.out.println("/------------------------");
        FormulaBuilder f2 = new FormulaBuilder("$x*($x+1)^2");
        f2.setVariable("$x", 2);
        System.out.println("/------------------------");
        System.out.println("f($x) = " + f2.toString());
        System.out.println("f(2) = " + f2.result());
        System.out.println("/------------------------");
    }
    
}