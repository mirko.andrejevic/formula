package org.formula;

/**
 *
 * @author MAndrejevic
 */
public class FormulaRoot implements Operation{

    private Operation _child;
    
    public FormulaRoot(){
        _child = null;
    }
    
    @Override
    public double result(){
        return _child.result();
    }

    @Override
    public OpType type() {
        return OpType.ROOT;
    }

    @Override
    public void setParent(Operation iParentOp){
        throw new FormulaException("Programmierfehler:"
                + " FormulaRoot.setParent aufgerufen");
    }

    @Override
    public int rank() {
        return 5;
    }
    
    @Override
    public void insertNewOperation(Operation iNewOp){
        if(_child == null){
            _child = iNewOp;
            _child.setParent(this);
        }else{
            if(iNewOp.type() == OpType.CLOSEBRACKET){
                throw new FormulaException("zu schließender Klammer gehört"
                + " keine öffnende Klammer");
            }else if(iNewOp.type() == OpType.COMMA){
                throw new FormulaException("Komma außerhalb von"
                        + " Funktionsauswertung");
            }else{
                iNewOp.insertNewOperation(_child);
                _child = iNewOp;
                _child.setParent(this);
            }
        }
    }

    @Override
    public void validateEnd(){
        if(_child == null){
            throw new FormulaException("Die Formel ist leer.");
        }
        
        _child.validateEnd();
    }

}
