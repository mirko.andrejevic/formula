package org.formula;

/**
 *
 * @author MAndrejevic
 */
public class CloseBracket implements Operation{

    private Operation _parent;
    
    public CloseBracket(){
        _parent = null;
    }
    
    @Override
    public double result(){
        throw new FormulaException("Programmierfehler:"
                    + " CloseBracket.result wurde aufgerufen.");
    }

    @Override
    public OpType type() {
        return OpType.CLOSEBRACKET;
    }

    @Override
    public void setParent(Operation iParentOp) {
        _parent = iParentOp;
    }

    @Override
    public int rank() {
        return 5;
    }
    
    @Override
    public void insertNewOperation(Operation iNewOp){
        try{
            _parent.insertNewOperation(iNewOp);
        }catch(NullPointerException e){
            throw new FormulaException("Programmierfehler:"
                    + " CloseBracket.insertNewOperation wurde aufgerufen, bevor"
                    + " diese Instanz in den Formel-Baum eingefügt wurde.");
        }
    }

    @Override
    public void validateEnd(){
        throw new FormulaException("Programmierfehler:"
                    + " CloseBracket.validateEnd wurde aufgerufen.");
    }

}
