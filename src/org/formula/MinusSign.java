package org.formula;

/**
 *
 * @author MAndrejevic
 */
public class MinusSign implements Operation{

    private Operation _parent;
    private Operation _child;
    
    public MinusSign(){
        _parent = null;
        _child = null;
    }
    
    @Override
    public double result(){
        return -_child.result();
    }

    @Override
    public OpType type() {
        return OpType.MINUSSIGN;
    }

    @Override
    public void setParent(Operation iParentOp) {
        _parent = iParentOp;
    }
    
    @Override
    public int rank() {
        return 1;
    }
    
    @Override
    public void insertNewOperation(Operation iNewOp){
        try{
            if(_child == null){
                _child = iNewOp;
                _child.setParent(this);
            }else{
                _parent.insertNewOperation(iNewOp);
            }
        }catch(NullPointerException e){
            throw new FormulaException("Programmierfehler:"
                    + " MinusSign.insertNewOperation wurde zweimal aufgerufen,"
                    + " bevor diese Instanz in den Formel-Baum eingefügt wurde.");
        }
    }

    @Override
    public void validateEnd(){
        if(_child == null){
            throw new FormulaException("Die Formel endet mit '-'.");
        }
            
        _child.validateEnd();
    }

}
