package org.formula;

/**
 *
 * @author MAndrejevic
 */
public class Subtraction implements Operation{

    private Operation _parent;
    private Operation _left;
    private Operation _right;
    
    public Subtraction(){
        _parent = null;
        _left = null;
        _right = null;
    }
    
    @Override
    public double result(){
        return _left.result() - _right.result();
    }

    @Override
    public OpType type() {
        return OpType.SUBTRACTION;
    }

    @Override
    public void setParent(Operation iParentOp) {
        _parent = iParentOp;
    }
    
    @Override
    public int rank() {
        return 4;
    }

    @Override
    public void insertNewOperation(Operation iNewOp){
        if(_left == null){
            _left = iNewOp;
            _left.setParent(this);
        }else if(_right == null){
            _right = iNewOp;
            _right.setParent(this);
        }else{
            if(this.rank() <= iNewOp.rank()){
                _parent.insertNewOperation(iNewOp);
            }else{
                iNewOp.insertNewOperation(_right);
                _right = iNewOp;
                _right.setParent(this);
            }
        }
    }

    @Override
    public void validateEnd(){
        if(_right == null){
            throw new FormulaException("Die Formel endet mit '-'.");
        }
            
        _right.validateEnd();
    }
    
}
