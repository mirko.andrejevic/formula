package org.formula;

/**
 *
 * @author MAndrejevic
 */
public class Function implements Operation{

    private Operation _parent;
    private Operation[] _args;
    private FunctionValue _function;
    private boolean _closed;
    private int _argCount;
    
    public Function(String iName){
        _parent = null;
        _closed = false;
        _argCount = 0;
        
        if("exp".equals(iName)){
            _function = new ExpFunc();
        }else if("ln".equals(iName)){
            _function = new LnFunc();
        }else if("round".equals(iName)){
            _function = new RoundFunc();
        }else if("floor".equals(iName)){
            _function = new FloorFunc();
        }else if("ceil".equals(iName)){
            _function = new CeilFunc();
        }else{
            throw new FormulaException("Die Funktion '" + iName
                    + "' ist unbekannt.");
        }
        
        _args = new Operation[_function.numOfArgs()];
    }
    
    @Override
    public double result(){
        double[] args = new double[_args.length];
        
        for(int i = 0; i < _args.length; i++){
            args[i] = _args[i].result();
        }
        
        return _function.calculate(args);
    }

    @Override
    public OpType type() {
        return OpType.FUNCTION;
    }

    @Override
    public void setParent(Operation iParentOp) {
        _parent = iParentOp;
    }

    @Override
    public int rank() {
        return 0;
    }
    
    @Override
    public void insertNewOperation(Operation iNewOp){
        if(_closed){
            _parent.insertNewOperation(iNewOp);
        }else if(iNewOp.type() == OpType.CLOSEBRACKET){
            if(_argCount < _args.length - 1){
                throw new FormulaException("Die Funktion '"
                        + _function.getName() + "' benötigt " + _args.length
                        + " Argumente.");
            }else if(_args[_argCount] == null){
                throw new FormulaException("Funktion '" + _function.getName()
                        + "': Das letzte Argument fehlt.");
            }else{
                iNewOp.setParent(this);
                _closed = true;
            }
        }else if(iNewOp.type() == OpType.COMMA){
            if(_argCount == _args.length - 1){
                throw new FormulaException("Die Funktion '"
                        + _function.getName() + "' benötigt " + _args.length
                        + " Argumente.");
            }else if(_args[_argCount] == null){
                throw new FormulaException("Funktion '" + _function.getName()
                        + "': Das " + (_argCount+1) + ". Argument fehlt.");
            }else{
                iNewOp.setParent(this);
                _argCount++;
            }
        }else if(_args[_argCount] == null){
            _args[_argCount] = iNewOp;
            _args[_argCount].setParent(this);
        }else{
            iNewOp.insertNewOperation(_args[_argCount]);
            _args[_argCount] = iNewOp;
            _args[_argCount].setParent(this);
        }
    }
    
    @Override
    public void validateEnd(){
        if(_args[0] == null){
            throw new FormulaException("Die Formel endet mit '('.");
        }else if(!_closed){
            throw new FormulaException("Die Funktionsauswertung von '"
                    + _function.getName() + "' endet nicht mit einer"
                    + " schließenden Klammer.");
        }
    }
    
    public static String readNextFunction(String iFormula, int iStringPos){
        int funcEnd = iStringPos + 1;
        char next;
        
        while(funcEnd < iFormula.length()){
            next = iFormula.charAt(funcEnd);
            if(('A' <= next && next <= 'Z') || ('a' <= next && next <= 'z')
                    || ('0' <= next && next <= '9') || next == '_'){
                funcEnd++;
            }else if(next == '('){
                break; //hängt die öffnende Klammer NICHT an den Namen
            }else{
                throw new FormulaException("Zeichen " + (funcEnd+1)
                            + ": Nach Funktionsname folgt keine öffnende Klammer.");
            }
        }
        
        return iFormula.substring(iStringPos, funcEnd);
    }

}

interface FunctionValue{
    
    int numOfArgs();
    double calculate(double[] args);
    String getName();
    
}

class ExpFunc implements FunctionValue{

    @Override
    public int numOfArgs(){
        return 1;
    }
    
    @Override
    public double calculate(double[] args){
        return Math.exp(args[0]);
    }

    @Override
    public String getName() {
        return "exp";
    }
}

class LnFunc implements FunctionValue{

    @Override
    public int numOfArgs(){
        return 1;
    }
    
    @Override
    public double calculate(double[] args){
        return Math.log(args[0]);
    }

    @Override
    public String getName() {
        return "ln";
    }
}

class RoundFunc implements FunctionValue{

    @Override
    public int numOfArgs(){
        return 1;
    }
    
    @Override
    public double calculate(double[] args){
        return Math.rint(args[0]);
    }

    @Override
    public String getName() {
        return "round";
    }
}

class FloorFunc implements FunctionValue{

    @Override
    public int numOfArgs(){
        return 1;
    }
    
    @Override
    public double calculate(double[] args){
        return Math.floor(args[0]);
    }

    @Override
    public String getName() {
        return "floor";
    }
}

class CeilFunc implements FunctionValue{

    @Override
    public int numOfArgs(){
        return 1;
    }
    
    @Override
    public double calculate(double[] args){
        return Math.ceil(args[0]);
    }

    @Override
    public String getName() {
        return "ceil";
    }
}
