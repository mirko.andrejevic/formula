package org.formula;

import java.util.HashMap;

/**
 *
 * @author MAndrejevic
 */
public class FormulaBuilder {
    
    private HashMap<String, VariableValue> _variables;
    private Operation _root;
    private Operation _last;
    private String _formula;
    private int _stringPos;
    
    public FormulaBuilder(String iFormula){
        _root = new FormulaRoot();
        _variables = new HashMap<String, VariableValue>();
        _last = _root;
        _formula = iFormula;
        _stringPos = 0;
        
        Operation newOp;
        while((newOp = compNextToken()) != null){
            _last.insertNewOperation(newOp);
            _last = newOp;
        }
        
        _root.validateEnd();
    }
    
    public String toString(){
        return this._formula;
    }
    
    public double result(){
        return _root.result();
    }
    
    public boolean hasVariables(){
        return !_variables.isEmpty();
    }
    
    public void setVariable(String iName, double iValue){
        iName = iName.toLowerCase();
        VariableValue var = _variables.get(iName);
        if(var == null){
            throw new FormulaException("Die Variable " + iName
                    + " kommt in der Formel nicht vor.");
        }else{
            var.setValue(iValue);
        }
    }
    
    private Operation compNextToken(){
        while(_stringPos < _formula.length()){
            if(_formula.charAt(_stringPos) == ' '){
                _stringPos++;
            }else{
                break;
            }
        }
        
        if(_stringPos == _formula.length()){
            return null;
        }
        
        OpType type = nextType();
        switch(type){
            case SCALAR:
                return makeScalar();
            case CONSTANT:
                return makeConstant();
            case VARIABLE:
                return makeVariable();
            case PLUSSIGN:
                _stringPos++;
                return new PlusSign();
            case MINUSSIGN:
                _stringPos++;
                return new MinusSign();
            case ADDITION:
                _stringPos++;
                return new Addition();
            case SUBTRACTION:
                _stringPos++;
                return new Subtraction();
            case MULTIPLICATION:
                _stringPos++;
                return new Multiplication();
            case DIVISION:
                _stringPos++;
                return new Division();
            case POWER:
                _stringPos++;
                return new Power();
            case MODULODIVISION:
                _stringPos++;
                return new ModuloDivision();
            case BRACKETS:
                _stringPos++;
                return new Brackets();
            case FUNCTION:
                return makeFunction();
            case CLOSEBRACKET:
                _stringPos++;
                return new CloseBracket();
            case COMMA:
                _stringPos++;
                return new Comma();
            default:
                throw new FormulaException("Programmierfehler: "
                    + "default-Zweig in FormulaBuilder.compNextToken erreicht.");
        }
    }
    
    private OpType nextType(){
        char next = _formula.charAt(_stringPos);
        
        try{
            if(('0' <= next && next <= '9') || next == '.'){
                return validateScalar();
            }else if(next == '@'){
                return validateConstant();
            }else if(next == '$'){
                return validateVariable();
            }else if(next == '+'){
                return validatePlus();
            }else if(next == '-'){
                return validateMinus();
            }else if(next == '*'){
                return validateMult();
            }else if(next == '/'){
                return validateDivision();
            }else if(next == '^'){
                return validatePower();
            }else if(next == '%'){
                return validateModulo();
            }else if(next == '('){
                return validateBrackets();
            }else if(('A' <= next && next <= 'Z') || ('a' <= next && next <= 'z')){
                return validateFunction();
            }else if(next == ')'){
                return validateClose();
            }else if(next == ','){
                return validateComma();
            }else{
                throw new FormulaException("Zeichen " + (_stringPos+1)
                        + ": Das Zeichen '" + next + "' ist in einer Formel"
                        + " nicht zulaessig.");
            }
        } catch (IndexOutOfBoundsException e){
            throw new FormulaException("Programmierfehler: "
                    + "IndexOutOfBoundsException in FormulaBuilder.nextType "
                    + "gefangen.");
        }
    }
    
    private OpType validateScalar(){
        switch(_last.type()){
            case SCALAR:
            case CONSTANT:
            case VARIABLE:
            case CLOSEBRACKET:
                throw new FormulaException("Zeichen " + (_stringPos+1)
                        + ": Ein Zahlenwert ist an "
                        + "dieser Stelle nicht zulaessig.");
            default:
                return OpType.SCALAR;
        }
    }

    
    private OpType validateConstant(){
        switch(_last.type()){
            case SCALAR:
            case CONSTANT:
            case VARIABLE:
            case CLOSEBRACKET:
                throw new FormulaException("Zeichen " + (_stringPos+1)
                        + ": Eine Konstante ist an "
                        + "dieser Stelle nicht zulaessig.");
            default:
                return OpType.CONSTANT;
        }
    }
    
    private OpType validateVariable(){
        switch(_last.type()){
            case SCALAR:
            case CONSTANT:
            case VARIABLE:
            case CLOSEBRACKET:
                throw new FormulaException("Zeichen " + (_stringPos+1)
                        + ": Eine Variable ist an "
                        + "dieser Stelle nicht zulaessig.");
            default:
                return OpType.VARIABLE;
        }
    }

    private OpType validatePlus() {
        switch(_last.type()){
            case SCALAR:
            case CONSTANT:
            case VARIABLE:
            case CLOSEBRACKET:
                return OpType.ADDITION;
            default:
                return OpType.PLUSSIGN;
        }
    }
    
    private OpType validateMinus() {
        switch(_last.type()){
            case SCALAR:
            case CONSTANT:
            case VARIABLE:
            case CLOSEBRACKET:
                return OpType.SUBTRACTION;
            default:
                return OpType.MINUSSIGN;
        }
    }
    
    private OpType validateMult(){
        switch(_last.type()){
            case SCALAR:
            case CONSTANT:
            case VARIABLE:
            case CLOSEBRACKET:
                return OpType.MULTIPLICATION;
            default:
                throw new FormulaException("Zeichen " + (_stringPos+1)
                        + ": Die Operation * (Multiplikation) ist an "
                        + "dieser Stelle nicht zulaessig.");
        }
    }

    private OpType validateDivision(){
        switch(_last.type()){
            case SCALAR:
            case CONSTANT:
            case VARIABLE:
            case CLOSEBRACKET:
                return OpType.DIVISION;
            default:
                throw new FormulaException("Zeichen " + (_stringPos+1)
                        + ": Die Operation / (Division) ist an "
                        + "dieser Stelle nicht zulaessig.");
        }
    }

    private OpType validatePower(){
        switch(_last.type()){
            case SCALAR:
            case CONSTANT:
            case VARIABLE:
            case CLOSEBRACKET:
                return OpType.POWER;
            default:
                throw new FormulaException("Zeichen " + (_stringPos+1)
                        + ": Die Operation ^ (Potenzierung) ist an "
                        + "dieser Stelle nicht zulaessig.");
        }
    }
    
    private OpType validateModulo(){
        switch(_last.type()){
            case SCALAR:
            case CONSTANT:
            case VARIABLE:
            case CLOSEBRACKET:
                return OpType.MODULODIVISION;
            default:
                throw new FormulaException("Zeichen " + (_stringPos+1)
                        + ": Die Operation % (Modulodivision) ist an "
                        + "dieser Stelle nicht zulaessig.");
        }
    }

    private OpType validateBrackets(){
        switch(_last.type()){
            case SCALAR:
            case CONSTANT:
            case VARIABLE:
            case CLOSEBRACKET:
                throw new FormulaException("Zeichen " + (_stringPos+1)
                        + ": Eine Oeffnende Klammer ist an "
                        + "dieser Stelle nicht zulaessig.");
            default:
                return OpType.BRACKETS;
        }
    }

    private OpType validateFunction(){
        switch(_last.type()){
            case SCALAR:
            case CONSTANT:
            case VARIABLE:
            case CLOSEBRACKET:
                throw new FormulaException("Zeichen " + (_stringPos+1)
                        + ": Ein Funktionsauswertung ist an "
                        + "dieser Stelle nicht zulaessig.");
            default:
                return OpType.FUNCTION;
        }
    }

    private OpType validateClose() {
        switch(_last.type()){
            case SCALAR:
            case CONSTANT:
            case VARIABLE:
            case CLOSEBRACKET:
                return OpType.CLOSEBRACKET;
            default:
                throw new FormulaException("Zeichen " + (_stringPos+1)
                        + ": Eine schliessende Klammer ist an "
                        + "dieser Stelle nicht zulaessig.");
        }
    }

    private OpType validateComma(){
        switch(_last.type()){
            case SCALAR:
            case CONSTANT:
            case VARIABLE:
            case CLOSEBRACKET:
                return OpType.COMMA;
            default:
                throw new FormulaException("Zeichen " + (_stringPos+1)
                        + ": Ein Komma ist an "
                        + "dieser Stelle nicht zulaessig.");
        }
    }
    
    private Operation makeScalar(){
        String scal = Scalar.readNextScalar(_formula, _stringPos);
        _stringPos += scal.length();
        return new Scalar(Double.parseDouble(scal));
    }

    private Operation makeConstant(){
        String cons = Constant.readNextConstant(_formula, _stringPos).toLowerCase();
        _stringPos += cons.length();
        return new Constant(cons);
    }

    private Operation makeVariable() {
        VariableValue varVal;
        String var = Variable.readNextVariable(_formula, _stringPos).toLowerCase();
        _stringPos += var.length();
        varVal = _variables.get(var);
        
        if(varVal == null){
            varVal = new VariableValue(var);
            _variables.put(var, varVal);
        }
        
        return new Variable(varVal);
    }

    private Operation makeFunction(){
        String func = Function.readNextFunction(_formula, _stringPos).toLowerCase();
        _stringPos += func.length() + 1; //Oeffnende Klammer Ueberspringen
        return new Function(func);
    }
}
