package org.formula;

/**
 *
 * @author MAndrejevic
 */
public class Constant implements Operation{
    
    private Operation _parent;
    private double _value;

    Constant(String iCons){
        _parent = null;
        
        if("@pi".equals(iCons)){
            _value = Math.PI;
        }else if("@e".equals(iCons)){
            _value = Math.E;
        }else{
            throw new FormulaException("Die Konstante '" + iCons + "' ist unbekannt.");
        }
    }
    
    @Override
    public double result() {
        return _value;
    }

    @Override
    public OpType type() {
        return OpType.CONSTANT;
    }

    @Override
    public void setParent(Operation iParentOp) {
        _parent = iParentOp;
    }
    
    @Override
    public int rank() {
        return 0;
    }

    @Override
    public void insertNewOperation(Operation iNewOp){
        try{
            _parent.insertNewOperation(iNewOp);
        }catch(NullPointerException e){
            throw new FormulaException("Programmierfehler:"
                    + " Constant.insertNewOperation wurde aufgerufen, bevor"
                    + " diese Instanz in den Formel-Baum eingefügt wurde.");
        }
    }
    
    @Override
    public void validateEnd(){} //Formel ok

    public static String readNextConstant(String iFormula, int iStringPos){
        int constEnd = iStringPos + 1;
        char next;
        
        while(constEnd < iFormula.length()){
            next = iFormula.charAt(constEnd);
            if(('A' <= next && next <= 'Z') || ('a' <= next && next <= 'z')
                    || ('0' <= next && next <= '9') || next == '_'){
                constEnd++;
            }else{
                break;
            }
        }
        
        return iFormula.substring(iStringPos, constEnd);
    }

}
