package org.formula;

/**
 *
 * @author MAndrejevic
 */
public class VariableValue {
    
    private double _value;
    private String _name;
    
    public VariableValue(String iName){
        _value = Double.NaN;
        _name = iName;
    }
    
    public double getValue(){
        return _value;
    }
    
    public void setValue(double iVal){
        _value = iVal;
    }
    
    public String getName(){
        return _name;
    }
    
}
