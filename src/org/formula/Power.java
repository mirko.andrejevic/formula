package org.formula;

/**
 *
 * @author MAndrejevic
 */
public class Power implements Operation{

    private Operation _parent;
    private Operation _base;
    private Operation _exponent;
    
    public Power(){
        _parent = null;
        _base = null;
        _exponent = null;
    }
    
    @Override
    public double result(){
        return Math.pow(_base.result(), _exponent.result());
    }

    @Override
    public OpType type() {
        return OpType.POWER;
    }

    @Override
    public void setParent(Operation iParentOp) {
        _parent = iParentOp;
    }
    
    @Override
    public int rank() {
        return 2;
    }

    @Override
    public void insertNewOperation(Operation iNewOp){
        if(_base == null){
            _base = iNewOp;
            _base.setParent(this);
        }else if(_exponent == null){
            _exponent = iNewOp;
            _exponent.setParent(this);
        }else{
            if(this.rank() <= iNewOp.rank()){
                _parent.insertNewOperation(iNewOp);
            }else{
                iNewOp.insertNewOperation(_exponent);
                _exponent = iNewOp;
                _exponent.setParent(this);
            }
        }
    }

    @Override
    public void validateEnd(){
        if(_exponent == null){
            throw new FormulaException("Die Formel endet mit '^'.");
        }
            
        _exponent.validateEnd();
    }

}
