package org.formula;

/**
 *
 * @author MAndrejevic
 */
public interface Operation {
    
    double result();
    OpType type();
    void setParent(Operation iParentOp);
    void insertNewOperation(Operation iNewOp);
    int rank();
    void validateEnd();
    
}
