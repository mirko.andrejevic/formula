package org.formula;

/**
 *
 * @author MAndrejevic
 */
public class FormulaException extends RuntimeException{
    
    public FormulaException(String message){
        super(message);
    }
    
}
